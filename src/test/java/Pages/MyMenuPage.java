package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MyMenuPage extends BasePage {
    public MyMenuPage(WebDriver driver){
        super(driver);
    }
    private static final String USER_NAME_LINK = "a.item[href='/mymenu/']";
    private static final String EXIT_BUTTON = "a.item[href*='exit']";
    private static final String PASSWORD_INPUT = "#emailloginform-password";
    private static final String SUBMIT_BUTTON = "[type='submit']";



    @FindBy(css=USER_NAME_LINK)
    private WebElement userNameLink;
    public WebElement getUserNameLink() {
        return userNameLink;
    }

    @FindBy(css=EXIT_BUTTON)
    private WebElement exitButton;
    public WebElement getExitButton() {
        return exitButton;
    }
    public HomePage logout(){
        getExitButton().click();
        return new HomePage(driver);
    }
    public void shouldDisplayUserName(String userName){
        assertThat(getUserNameLink().getText(), equalTo(userName));
    }
    public void shouldHaveExitButton() {
        assertThat("Exit button should be displayed",getExitButton().isDisplayed());
    }
}