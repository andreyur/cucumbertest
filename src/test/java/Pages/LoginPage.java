package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.assertThat;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver driver){
        super(driver);
    }
    private static final String LOGIN_FORM = "div.login-block";
    private static final String EMAIL_INPUT = "input#emailloginform-email";
    private static final String PASSWORD_INPUT = "#emailloginform-password";
    private static final String SUBMIT_BUTTON = "[type='submit']";

    @FindBy(css=LOGIN_FORM)
    private WebElement loginForm;
    public WebElement getLoginForm() {
        return loginForm;
    }

    @FindBy (css=EMAIL_INPUT)
    private WebElement emailInput;
    public WebElement getEmailInput() {
        return emailInput;
    }

    @FindBy (css=PASSWORD_INPUT)
    private WebElement passwordInput;
    public WebElement getPasswordInput() {
        return passwordInput;
    }

    @FindBy (css=SUBMIT_BUTTON)
    private WebElement submitButton;
    public WebElement getSubmitButton() {
        return submitButton;
    }
    public void shouldHaveLoginForm(){
        assertThat("Login Form should be displayed",getLoginForm().isDisplayed());
    }
    public void login(String userName, String password){
        driver.switchTo().frame(0);
        getEmailInput().sendKeys(userName);
        getPasswordInput().sendKeys(password);
        getSubmitButton().click();
    }
    public void shouldHaveEmailInput(){
        driver.switchTo().frame(0);
        assertThat("Email Input should be displayed",getEmailInput().isDisplayed());
        driver.switchTo().defaultContent();
    }
    public void shouldHavePasswordInput(){
        driver.switchTo().frame(0);
        assertThat("Password Input should be displayed",getPasswordInput().isDisplayed());
        driver.switchTo().defaultContent();
    }
}
