package Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;
import java.util.Random;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SearchResultsPage extends BasePage {
    public SearchResultsPage(WebDriver driver){
        super(driver);
    }

    public static final String SECTION = "section.ticket-item";
    public static final String SEARCH_RESULT_PHOTO = "section.ticket-item div.ticket-photo";

    @FindBy(css = SECTION)
    private List<WebElement> listSearch;
    public List<WebElement> getListSearch() {
        return listSearch;
    }

    @FindBy(css=SEARCH_RESULT_PHOTO)
    private List<WebElement> searchResultPhoto;
    public List<WebElement> getSearchResultPhoto() {
        return searchResultPhoto;
    }

    public void shouldHaveSearchResults(int searchResults){
        assertThat(getListSearch(), hasSize(21));
    }

    public void shouldHaveBrandAndModelInSearchResults(String brand, String model){
        List<WebElement> list = getListSearch();
        for (WebElement element : list) {
            assertThat(element.getText(), is(containsString(brand+" "+model)));
        }
    }

    public ProductPage clickRandomSearchResult() {
        WebElement element = getSearchResultPhoto().get(new Random().nextInt(getListSearch().size()));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        element.click();
        return new ProductPage(driver);
    }
}
