package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.MatcherAssert.assertThat;

public class Footer  extends BasePage {
    public Footer(WebDriver driver){
        super(driver);
    }
    private static final String COPYRIGHT_P = "p.copyright";
    private static final String INSTAGRAM_LINK = "a.i-btn-instagram";
    private static final String TWITTER_LINK = "a.i-btn-twitter";
    private static final String FACEBOOK_LINK = "a.i-btn-facebook";
    private static final String YOUTUBE_LINK = "a.i-btn-youtube";
    private static final String OK_LINK = "a.i-btn-ok";
    private static final String VK_LINK = "a.i-btn-vk";


    @FindBy(css=COPYRIGHT_P)
    private WebElement copyrightP;
    public WebElement getCopyrightP() {
        return copyrightP;
    }

    @FindBy(css=INSTAGRAM_LINK)
    private WebElement instagramLink;
    public WebElement getInstagramLink() {
        return instagramLink;
    }

    @FindBy(css=TWITTER_LINK)
    private WebElement twitterLink;
    public WebElement getTwitterLink() {
        return twitterLink;
    }

    @FindBy(css=FACEBOOK_LINK)
    private WebElement facebookLink;
    public WebElement getFacebookLink() {
        return facebookLink;
    }

    @FindBy(css=YOUTUBE_LINK)
    private WebElement youtubeLink;
    public WebElement getYoutubeLink() {
        return youtubeLink;
    }

    @FindBy(css=OK_LINK)
    private WebElement vkLink;
    public WebElement getVkLink() {
        return vkLink;
    }

    @FindBy(css=VK_LINK)
    private WebElement okLink;
    public WebElement getOkLink() {
        return okLink;
    }

    public void scrollToCopyright(){
        Actions actions = new Actions(driver);
        actions.moveToElement(getCopyrightP());
        actions.perform();
    }

    public void shouldHaveInstagramLink(){
        assertThat("Instagram link should be displayed",getInstagramLink().isDisplayed());
    }

    public void shouldHaveTwitterLink(){
        assertThat("Twitter link should be displayed",getTwitterLink().isDisplayed());
    }

    public void shouldHaveFacebookLink(){
        assertThat("Facebook link should be displayed",getFacebookLink().isDisplayed());
    }

    public void shouldHaveYoutubeLink(){
        assertThat("Youtube link should be displayed",getYoutubeLink().isDisplayed());
    }

    public void shouldHaveOkLink(){
        assertThat("OK link should be displayed",getOkLink().isDisplayed());
    }

    public void shouldHaveVkLink(){
        assertThat("VK link should be displayed",getVkLink().isDisplayed());
    }
}
