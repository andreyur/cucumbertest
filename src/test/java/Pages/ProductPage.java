package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver){
        super(driver);
    }
    private static final String HEADING_H1 = "h1.head[title]";
    private static final String NEXT_PRODUCT_LINK = "span.next_advert";
    private static final String HEADING_H3 = "h3.auto-content_title";


    @FindBy(css=HEADING_H1)
    private WebElement headingH1;
    public WebElement getHeadingH1() {
        return headingH1;
    }

    @FindBy(css=NEXT_PRODUCT_LINK)
    private WebElement nextProductLink;
    public WebElement getNextProductLink() {
        return nextProductLink;
    }

    @FindBy(css=HEADING_H3)
    private WebElement headingH3;
    public WebElement getHeadingH3() {
        return headingH3;
    }

    public void shouldHaveBrandAndModelInHeadingH1(String brand, String model){
        assertThat(getHeadingH1().getText(), is(containsString(brand+" "+model)));
    }

    public void shouldHaveBrandAndModelInNextProductLink(String brand, String model){
        assertThat(getNextProductLink().getText(), is(containsString(brand+" "+model)));
    }

    public void shouldHaveBrandAndModelInHeadingH3(String brand, String model){
        assertThat(getHeadingH3().getText(), is(containsString(brand+" "+model)));
    }
}