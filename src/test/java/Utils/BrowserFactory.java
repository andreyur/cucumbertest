package Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class BrowserFactory {
    public static WebDriver getDriver (String name){
        switch (name){
            case "FIREFOX":
                System.setProperty("webdriver.gecko.driver","src/test/resources/geckodriver.exe");
                return new FirefoxDriver();
            case "CHROME":
                System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver.exe");
                return new ChromeDriver();
            case "EDGE":
                System.setProperty("webdriver.edge.driver","src/test/resources/msedgedriver.exe");
                return new EdgeDriver();
            default:
                System.setProperty("webdriver.gecko.driver","src/test/resources/geckodriver.exe");
                return new FirefoxDriver();
        }
    }
}
