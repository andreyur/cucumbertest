package CucumberTest;

import Pages.*;
import Utils.BrowserFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import java.net.URISyntaxException;

public class StepDefinitions {
    WebDriver driver;
    HomePage homePage;
    LoginPage loginPage;
    MyMenuPage myMenuPage;
    SearchResultsPage searchResultsPage;
    ProductPage productPage;
    Footer footer;
    @Before
    public void setUp() {
        // Initialization WebDriver
        driver= BrowserFactory.getDriver("CHROME");
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        myMenuPage = new MyMenuPage(driver);
        searchResultsPage = new SearchResultsPage(driver);
        productPage = new ProductPage(driver);
        footer = new Footer(driver);
    }
    @Given("^User opened the browser$")
    public void user_opened_the_browser() {

    }
    @When("^HomePage is opened$")
    public void homePage_is_opened() {
        homePage.open(HomePage.BASE_URL);
    }
    @Then("^HomePage has a host (.+)$")
    public void homePage_has_a_host(String hostName) throws URISyntaxException {
        homePage.shouldHaveHost(hostName);
    }@And("^HomePage has a title (.+)$")
    public void homepageHasATitleTitle(String title) {
        homePage.shouldHaveTitle(title);
    }
    @And("^HomePage has a logo$")
    public void homepageHasALogo() {
        homePage.shouldHaveLogo();
    }
    @And("^HomePage has a search form$")
    public void homepageHasASearchForm() {
        homePage.shouldHaveSearchForm();
    }
    @Given("^LoginPage is opened$")
    public void loginpageIsOpened() {
        homePage.open(HomePage.BASE_URL);
        homePage.clickLoginLink();
    }
    @When("^User logins with valid (.+) and (.+)$")
    public void userLoginsWithValidUsernameAndPassword(String username, String password) {
        loginPage.login(username, password);
    }
    @Then("^MyMenuPage is opened$")
    public void mymenupageIsOpened() {
        myMenuPage.shouldHaveExitButton();
    }
    @When("^User searches with (.+), (.+), (.+), (.+)$")
    public void userSearchesWithCategoryBrandModelYear(String category, String brand, String model, String year) {
        homePage.search(category, brand, model, year);
    }
    @Then("^There are 21 search results$")
    public void thereAreSearchResults(){
        searchResultsPage.shouldHaveSearchResults(21);
    }

    @And("^Each search result contains (.+) and (.+)$")
    public void eachSearchResultContainsBrandAndModel(String brand, String model){
         searchResultsPage.shouldHaveBrandAndModelInSearchResults(brand, model);
    }
    @When("^User clicks on random search result$")
    public void userClicksOnRandomSearchResult() {
         searchResultsPage.clickRandomSearchResult();
    }

    @Then("^Main heading contains (.+) and (.+)$")
    public void mainHeadingContainsBrandAndModel(String brand, String model) {
         productPage.shouldHaveBrandAndModelInHeadingH1(brand, model);
    }

    @And("^Next product link contains (.+) and (.+)$")
    public void nextProductLinkContainsBrandAndModel(String brand, String model) {
         productPage.shouldHaveBrandAndModelInNextProductLink(brand, model);
    }
    @When("^User scrolls the page to the bottom$")
    public void userScrollsThePageToTheBottom() {
        footer.scrollToCopyright();
    }

    @Then("^Footer has a Instagram share button$")
    public void footerHasAInstagramShareButton() {
         footer.shouldHaveInstagramLink();
    }

    @And("^Footer has a Twitters share button$")
    public void footerHasATwittersShareButton() {
         footer.shouldHaveTwitterLink();
    }

    @And("^Footer has a Facebook share button$")
    public void footerHasAFacebookShareButton() {
         footer.shouldHaveFacebookLink();
    }

    @And("^Footer has a Youtube share button$")
    public void footerHasAYoutubeShareButton() {
         footer.shouldHaveYoutubeLink();
    }

    @And("^Footer has a Ok share button$")
    public void footerHasAOkShareButton() {
         footer.shouldHaveOkLink();
    }

    @And("^Footer has a Vk share button$")
    public void footerHasAVkShareButton() {
         footer.shouldHaveVkLink();
    }
    @After
    public void tearDown() {
        //Close the browser
        driver.quit();
    }

}
