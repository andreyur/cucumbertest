Feature: Auto Tests

  Scenario Outline: Home Page Test
    Given User opened the browser
    When HomePage is opened
    Then HomePage has a host <hostName>
    And HomePage has a title <title>
    And HomePage has a logo
    And HomePage has a search form

  Examples:
  | hostName          | title                                                               |
  | auto.ria.com      | AUTO.RIA™ — Автобазар №1. Купити і продати авто легко як ніколи |

  @Login
  Scenario Outline: Login Test
    Given LoginPage is opened
    When User logins with valid <username> and <password>
    Then MyMenuPage is opened

    Examples:
      | username             | password   |
      | alushpigan@gmail.com | andrey12   |

  Scenario Outline: Search Test
    Given HomePage is opened
    When User searches with <category>, <brand>, <model>, <year>
    Then There are 21 search results
    And Each search result contains <brand> and <model>

    Examples:
      | category    | brand      | model   | year   |
      | Легкові    | Volkswagen | Jetta   | 2015   |
      | Легкові    | BMW        | X5      | 2016   |
      | Легкові    | Ford       | Fusion  | 2014   |

  Scenario Outline: Product Page Test
    Given HomePage is opened
    And User searches with <category>, <brand>, <model>, <year>
    When User clicks on random search result
    Then Main heading contains <brand> and <model>
    And Next product link contains <brand> and <model>

    Examples:
      | category    | brand      | model   | year   |
      | Легкові    | Volkswagen | Jetta   | 2015   |

  Scenario: ShareButtons On Footer Test
    Given HomePage is opened
    When User scrolls the page to the bottom
    Then Footer has a Instagram share button
    And Footer has a Twitters share button
    And Footer has a Facebook share button
    And Footer has a Youtube share button
    And Footer has a Ok share button
    And Footer has a Vk share button
